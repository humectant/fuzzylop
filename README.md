# fuzzylop

Fuzzylop creates dynamic shovels and federations in RabbitMQ and keeps them up to date using DNS for service discovery.

Use this program if:
* you want to automate configuration of Shovel and Federation in RabbitMQ
* shovel/federation source or destination endpoints are volatile. In other words, if a host changes this program will automatically correct the endpoints configuration in RabbitMQ.
* you want to prevent drift in your RabbitMQ Shovel and Federation configurations

The program uses DNS SRV records as an authoritative list of endpoints for RabbitMQ. If there are existing configurations in RabbitMQ the program will check if the endpoints are up to date. If not it will delete and recreate the configurations. Communication with RabbitMQ is through the HTTP API.

Caveat: This program is not much more than an exploration of RabbitMQ configuration through Python (circa 2016) and may very well be unnecessary and/or superseded by RabbitMQ releases after 3.6.12.

## Install

* Install Python 2
* Install dependencies
```
pip install requests
pip install dnspython
pip install yaml
```
* Install fuzzylop
```
git clone https://github.com/humectant/fuzzylop.git
```
Note: Ensure the RabbitMQ broker that the program will communicate with has the shovel and federation plugins. See RabbitMQ Federation and RabbitMQ Shovel for details.

## Run
Fuzzylop is written in Python.
```
python fuzzylop.py
```

There is help.
```
python fuzzylop.py -h
```

* The program will read the configuration file config_file. The argument is optional and defaults to fuzzylop.yml.
* The log level defaults to INFO is it isn't set. The options are DEBUG, INFO, WARN or ERROR
* It is expected that this program will get called by a scheduler such as cron.
* As it can access the source broker, destination broker and DNS the program can run anywhere.

## Using the Docker Image

* Pull the image from the default registry.
```
     docker pull humectant/fuzzylop
```
* Pull the image from your corporate registry.
```
     docker pull docker-registry:5000/humectant/fuzzylop
```
* In the example below /path/to/config/dir is a directory on the local filesystem that contains the configuration file fuzzylop.yml.
```
     docker run -t -i -v /path/to/config/dir:/config humectant/fuzzylop
```
* This will mount /path/to/config/dir into the running container.

## Configuration

The fuzzylop program reads RabbitMQ resource definitions from its configuration file. Resources consist of shovel and federation definitions. For each resource the program will do the following:
1. Get list of active RabbitMQ nodes from DNS SRV record
2. Check if the resource exists
3. If it exists and matches the nodes match do nothing. Unless force=True in which case delete it. If it exists and the hosts don't match
delete it.
4. Create a new resource with the latest list of nodes from DNS

The program communicates with RabbitMQ using the HTTP API.

If the file parameter (-f) isn't passed in on the command line, the program expects to find its properties in a file fuzzylop.yml. Behold the sample below.

```
# Base URI of RabbitMQ Management API where shovel/federation will be
     configured
     r_endpoint: http://somehost:15672
     # Vhost of broker where shovel/federation will be configured
     r_vhost: vhost8
     # Username of broker where shovel/federation will be configured
     r_username: joe
     # Password of broker where shovel/federation will be configured
     r_password: PASSWORD
     # Force update of shovel/federation upstream even if values are up to
     date
     force: False
     # Destroy everything immediately after it is created for testing
     purposes

   immed_delete: True
# Log level
log_level: DEBUG
# Timer in seconds. If 0 it exits immediately
timer: 0
resources:
  - !!python/object:__main__.ShovelConfig
    name: shoveler3
    type: Shovel
    vhost: vhost1
    # DNS endpoint to use for source URI if this is a shovel
    src_dns:
    # DNS endpoint to use for destination URI if this is a shovel
    dest_dns: _vlmcs._tcp.it.cornell.edu
    # DNS endpoint to use for usptream URI if this is federation
    upstream_dns:
    # These are the actual paramters passed to RabbitMQ. Many, if unset,
use defaults
    resource:
      # Even if DNS will replace this it needs to be supplied as it is
used as a template
      src-uri: amqp://joe:PASSWORD@somehost:5672/vhost8
      src-exchange: trials
      #src-queue:
      src-exchange-key: trial.event.hello
      # Even if DNS will replace this it needs to be supplied as it is
used as a template
      dest-uri: amqp://joe:PASSWORD@host1:123/vhost8
      # Use this and ignore DNS answers
      dest-exchange: trials
      #dest-queue:
      dest-exchange-key: trial.event.hello
      ack-mode: on-publish
      #prefetch-count: 1000
      #reconnect-delay: 1
      #publish-properties: {}
      #add-forward-headers: false
      #delete-after: never
  - !!python/object:__main__.UpstreamConfig
    name: federation3
    type: Upstream
    vhost: fabric
    # DNS endpoint to use for upstream URI if this is federation
    upstream_dns: _vlmcs._tcp.it.cornell.edu
    # These are the actual paramters passed to RabbitMQ. Many, if unset,
use defaults
    resource:
      # Even if DNS will replace this it needs to be supplied as it is
used as a template
      uri: amqp://host2:12345/vhost8
      expires: 3600000
      max-hops: 1
         trust-user-id: True
    policy_name: policy3
  - !!python/object:__main__.PolicyConfig
    name: policy3
    type: Policy
    vhost: fabric
    # These are the actual paramters passed to RabbitMQ. Many, if unset,
use defaults
    resource:
      pattern: trials
      definition:
        federation-upstream-set: all
apply-to: exchanges
```
In the sample above one shovel and a federation (one upstream and one policy) are defined. See RabbitMQ Federation and Configuring Dynamic Shovels for information about how to configure these resources.

## Monitoring
The program logs to standard out. Use a tool like Splunk to capture the output.

Below we have an example of a successful run with one shovel and one federation (an upstream and a policy defined). Logging is set to DEBUG.

```
2016-10-18 09:25:51,151 INFO #### Configure Shovel resource: shoveler3
2016-10-18 09:25:51,151 INFO Old Dest URI:
amqp://fabric:FqyN9eFF9WDdwuj3@host1:123/fabric
2016-10-18 09:25:51,164 INFO DNS answers from
_vlmcs._tcp.it.cornell.edu: ['kms02.cit.cornell.edu:1688',
'kms01.cit.cornell.edu:1688']
2016-10-18 09:25:51,165 INFO New Dest URI:
amqp://fabric:FqyN9eFF9WDdwuj3@kms02.cit.cornell.edu:1688/fabric
2016-10-18 09:25:51,165 INFO Find shoveler3 in vhost /fabric
2016-10-18 09:25:51,165 INFO Get resource: shoveler3
2016-10-18 09:25:51,587 INFO is_dns: None or _vlmcs._tcp.it.cornell.edu
2016-10-18 09:25:51,587 DEBUG Current resource: {u'vhost': u'fabric',
u'component': u'shovel', u'name': u'shoveler3', u'value': {u'src-uri':
u'amqp://fabric:fabric@192.168.0.4:5672/fabric', u'ack-mode':
u'on-publish', u'dest-uri':
u'amqp://fabric:FqyN9eFF9WDdwuj3@kms02.cit.cornell.edu:1688/fabric',
u'dest-exchange-key': u'trial.event.hello', u'src-exchange-key':
u'trial.event.hello', u'src-exchange': u'trials', u'dest-exchange':
u'trials'}}
2016-10-18 09:25:51,587 INFO #### Configure Upstream resource:
federation3
2016-10-18 09:25:51,587 INFO Old Upstream URI: amqp://host2:12345/fabric
2016-10-18 09:25:51,588 INFO New Upstream URI: amqp://host2:12345/fabric
2016-10-18 09:25:51,588 INFO Find federation3 in vhost /fabric
2016-10-18 09:25:51,588 INFO Get resource: federation3
2016-10-18 09:25:51,591 DEBUG Current resource: {u'vhost': u'fabric',
u'component': u'federation-upstream', u'name': u'federation3', u'value':
{u'expires': 3600000, u'trust-user-id': True, u'uri':
u'amqp://host2:12345/fabric', u'max-hops': 1}}
2016-10-18 09:25:51,591 INFO #### Configure Policy resource: policy3
2016-10-18 09:25:51,591 INFO Find policy3 in vhost /fabric
2016-10-18 09:25:51,591 INFO Get resource: policy3
2016-10-18 09:25:51,593 INFO Resource not found
2016-10-18 09:25:51,594 INFO Create resource: policy3 as {"pattern":
"trials", "apply-to": "exchanges", "definition":
{"federation-upstream-set": "all"}}
2016-10-18 09:25:51,594 INFO Put resource: policy3
2016-10-18 09:25:51,628 DEBUG Created resource
```

## Resilience

Use Publisher Confirms (ack_mode = on-confirm) to minimize message loss. With Publisher Confirms, where the client (i.e. Shovel) does not remove a message from a source queue until the target queue(s) report whether the message has been delivered. Publisher Confirms is enabled in both shovel and federation by default.

> Rabbit provides two plugins to assist with distributing nodes over unreliable networks: federation and theshovel. Both are implemented as AMQP clients, so if you configure them to use confirms and acknowledgements, they will retransmit when necessary. Both will use confirms and acknowledgements by default.

> When connecting clusters with federation or the shovel, it is desirable to ensure that the federation links and shovels tolerate node failures. Federation will automatically distribute links across the downstream cluster and fail them over on failure of a downstream node. In order to connect to a new upstream when an upstream node fails you can specify multiple redundant URIs for an upstream, or connect via a TCP load balancer.

> When using the shovel, it is possible to specify redundant brokers in a source or destination clause; however it is not currently possible to make the shovel itself redundant. We hope to improve this situation in the future; in the mean time a new node can be brought up manually to run a shovel if the node it was originally running on fails.

Note that shovel and federation settings are always cluster-wide. Shovels, however, only run on one node as a singleton. If a node goes down the shovel will move to another active node.
Developers should employ a DLX (Dead-letter exchange) and levarge TTL (time-to-live) to ensure messages aren't lost. Also see the RabbitMQ Reliability Guide

## Road Map

* Use async timer
* Allow definition of upstream sets for federation
* Fix problem where multiple Shovel targets doesn't work
* Resolve issue with RabbitMQ HTTP API only allowing one node for the dynamic Shovel configuration
* Pass in log level from Docker environment variable
