FROM alpine:3.1

MAINTAINER Ira Waxberg <humectant@waxberg.com>

RUN apk add --update python py-pip

RUN pip install requests
RUN pip install dnspython
RUN pip install pyaml

COPY fuzzylop.py /fuzzylop.py 

CMD ["python", "/fuzzylop.py", "-f", "/config/fuzzylop.yml"]