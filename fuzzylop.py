#!/usr/bin/env python

"""
    Purpose: Build dynamic RabbitMQ Shovel and Federation resources
"""

import sys, getopt, time
import requests
import json
import dns.resolver
import yaml
import logging
import re

try:
    import httplib
except ImportError:
    import http.client as httplib

# Logging
logging.basicConfig(format='%(asctime)-15s %(levelname)s %(message)s')
logger = logging.getLogger('fuzzylop')

# Low level logging
# httplib.HTTPConnection.debuglevel = 1
# requests_log = logging.getLogger("requests.packages.urllib3")
# requests_log.setLevel(logging.DEBUG)
# requests_log.propagate = True

# Cache of DNS answers
targets_cache = {}


# Base class for RabbitMQ resource configuration
class ResourceConfig:
    def __init__(self, name, vhost, src_dns, dest_dns, upstream_dns, resource):
        self.name = name
        self.type = type
        self.vhost = vhost
        self.src_dns = src_dns
        self.dest_dns = dest_dns
        self.upstream_dns = upstream_dns
        self.resource = resource
        self.config = None

    def set_config(self, config):
        self.config = config

    def delete(self):
        logger.info('Delete resource: ' + self.name)
        return (requests.delete(self.get_rabbit_uri(), auth=(self.config['r_username'],
                                                             self.config['r_password'])))

    def get(self):
        logger.info('Get resource: ' + self.name)
        return (requests.get(self.get_rabbit_uri(), auth=(self.config['r_username'],
                                                          self.config['r_password'])))

    def put(self, resource_json):
        logger.info('Put resource: ' + self.name)
        headers = {"Content-type": "application/json"}
        return (requests.put(self.get_rabbit_uri(), auth=(self.config['r_username'],
                                                          self.config['r_password']), headers=headers,
                             data=resource_json))

    def get_json(self):
        return json.dumps({'value': self.resource})

    def __repr__(self):
        return "%s(name=%r)" % (
            self.__class__.__name__, self.name)


# RabbitMQ Shovel resource configuration
class ShovelConfig(ResourceConfig):
    def get_rabbit_uri(self):
        return (self.config['r_endpoint'] + '/api/parameters/shovel/'
                + self.vhost + '/' + self.name)

    def is_dns(self):
        logger.info('is_dns: ' + str(self.src_dns) + ' or ' + str(self.dest_dns))
        if self.src_dns is not None or self.dest_dns is not None:
            return True

    def is_src_dns(self):
        if self.src_dns is not None:
            return True

    def is_dest_dns(self):
        if self.dest_dns is not None:
            return True

    def targets_match(self, curr_json):
        matched = True
        logger.debug('Current resource: ' + str(curr_json))
        if self.is_src_dns():
            old_src_uri = curr_json[u'value'][u'src-uri']
            new_src_uri = self.resource['src-uri']
            if old_src_uri != new_src_uri:
                logger.info('Source URIs differ. Current URI: ' +
                            old_src_uri + ', From DNS: ' + new_src_uri)
                matched = False
        if self.is_dest_dns():
            old_dest_uri = curr_json[u'value'][u'dest-uri']
            new_dest_uri = self.resource['dest-uri']
            if old_dest_uri != new_dest_uri:
                logger.info('Dest URIs differ. Current URI: ' +
                            old_dest_uri + ', From DNS: ' + new_dest_uri)
                matched = False
        return matched

    def refresh_targets(self):
        if self.is_src_dns():
            uri = self.resource['src-uri']
            logger.info('Old Source URI: ' + uri)
            self.resource['src-uri'] = replace_endpoints(uri, get_targets(self.src_dns, 'srv'))
            logger.info('New Source URI: ' + self.resource['src-uri'])
        if self.is_dest_dns():
            uri = self.resource['dest-uri']
            logger.info('Old Dest URI: ' + uri)
            self.resource['dest-uri'] = replace_endpoints(uri, get_targets(self.dest_dns, 'srv'))
            logger.info('New Dest URI: ' + str(self.resource['dest-uri']))


# RabbitMQ Upstream resource configurationA Federation is defined by an Upstream and a Policy
class UpstreamConfig(ResourceConfig):
    def get_rabbit_uri(self):
        return (self.config['r_endpoint'] + '/api/parameters/federation-upstream/'
                + self.vhost + '/' + self.name)

    def is_dns(self):
        if self.upstream_dns is not None:
            return True

    def is_upstream_dns(self):
        if self.upstream_dns is not None:
            return True

    def refresh_targets(self):
        if self.upstream_dns:
            uri = self.resource['uri']
            logger.info('Old Upstream URI: ' + uri)
            self.resource['uri'] = ",".join(replace_endpoints(uri, get_targets(self.upstream_dns, 'srv')))
            logger.info('New Upstream URI: ' + self.resource['uri'])

    def targets_match(self, curr_json):
        matched = True
        logger.debug('Current resource: ' + str(curr_json))
        if self.is_upstream_dns():
            old_upstream_uri = curr_json[u'value'][u'uri']
            new_upstream_uri = self.resource['uri']
            if old_upstream_uri != new_upstream_uri:
                logger.info('Usptream URIs differ. Current URI: ' +
                            old_upstream_uri + ', From DNS: ' + new_upstream_uri)
                matched = False
        return matched

    # RabbitMQ Policy resource configuration; A Federation is defined by an Upstream and a Policy


class PolicyConfig(ResourceConfig):
    def get_rabbit_uri(self):
        return (self.config['r_endpoint'] + '/api/policies/'
                + self.vhost + '/' + self.name)

    def is_dns(self):
        return False

    def get_json(self):
        return json.dumps(self.resource)

    def refresh_targets(self):
        pass


# Get DNS info
def get_targets(domain, dns_type):
    if domain in targets_cache:
        return targets_cache[domain]
    answers = dns.resolver.query(domain, dns_type)
    target_list = []
    for rdata in answers:
        # print 'AMQP target hosts: ', rdata.target, rdata.port
        target_list += [str(rdata.target)[: -1] + ':' + str(rdata.port)]
    logger.info('DNS answers from ' + domain + ': ' + str(target_list))
    targets_cache[domain] = target_list
    return target_list


# Update AMQP target hosts
def replace_endpoints(template_uri, targets):
    uris = []
    for target in targets:
        uris += [re.sub('(?<=@)(.*)(?=/)', target, template_uri)]
    return uris


def start(config):
    force = config['force']
    immed_delete = config['immed_delete']
    for res in config['resources']:  # Process resources

        logger.info('#### Configure ' + str(res.type) + ' resource: ' + str(res.name))
        res.set_config(config)
        refresh = False

        # Try to replace in-memory resource target(s) with DNS anwers
        res.refresh_targets()

        # Find 
        logger.info('Find ' + res.name + ' in vhost /' + res.vhost)
        response = res.get()
        if response.status_code == requests.codes.ok:
            # Resource exists;
            if force:  # Force delete
                logger.info('Force refresh')
                res.delete()
                refresh = True
            elif res.is_dns():  # If DNS value provided, compare existing Rabbit configs with DNS
                if not res.targets_match(response.json()):
                    res.delete()
                    refresh = True
            else:
                refresh = True
        else:
            logger.info('Resource not found')
            refresh = True

            # Create resource
        if refresh:

            resource_json = res.get_json()
            logger.info('Create resource: ' + res.name + ' as ' + str(resource_json))
            r = res.put(resource_json)
            if r.status_code != requests.codes.no_content:
                logger.error('Failed to create resource ' + str(r) + ', Response: ' + r.text)
            else:
                logger.debug('Created resource')

        # Set immediate delete which results in all resources getting deleted at program end
        if immed_delete:
            res.delete()


def main(argv):
    config_file = 'fuzzylop.yml'  # Default
    log_levels = {'DEBUG': 10, 'INFO': 20, 'WARN': 30, 'ERROR': 40}  # RabbitMQ codes
    log_level = None

    try:

        try:
            opts, args = getopt.getopt(argv, "f:l:h", ["config_file=", "log_level=", "help"])
        except getopt.GetoptError:
            print 'fuzzylop.py -f <config_file> -l <log_level>'
            sys.exit(2)
        for opt, arg in opts:
            if opt in ('-h', '--help'):
                print 'fuzzylop.py -f <config_file> -l <log_level>'
            elif opt in ("-f", "--config_file"):
                config_file = arg.strip()
            elif opt in ("-l", "--log_level"):
                log_level = int(log_levels[arg])

        # Load properties
        with open(config_file) as f:
            config = yaml.load(f)
            f.close()

        # Set log level
        try:
            if not log_level:
                log_level = log_levels[config['log_level']]
        except:
            log_level = log_levels['INFO']
        logger.setLevel(log_level)

        if 'timer' in config and config['timer'] != 0:
            while True:
                start(config)
                time.sleep(config['timer'])
        else:
            start(config)

    except Exception, e:
        logger.error(str(e))
        raise


if __name__ == '__main__':
    main(sys.argv[1:])
